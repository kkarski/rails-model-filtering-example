class CreateWorkplaces < ActiveRecord::Migration[5.2]
  def change
    create_table :workplaces do |t|
      t.string :name, null: false

      t.timestamps
    end

    add_index :workplaces, :name
  end
end
