class AddPersonRefToAddresses < ActiveRecord::Migration[5.2]
  def change
    add_reference :addresses, :person, foreign_key: true, index: true, null: true
  end
end
