class AddWorkplaceRefToPeople < ActiveRecord::Migration[5.2]
  def change
    add_reference :people, :workplace, foreign_key: true, index: true, null: true
  end
end
