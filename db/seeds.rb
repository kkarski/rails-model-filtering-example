# frozen_string_literal: true
# ruby encoding: utf-8
require 'faker'

puts 'Seeding database...'

people_list = []

50.times do
  people_list << { first_name: Faker::Name.first_name, last_name: Faker::Name.last_name, age: rand(21..46) }
end

address_list = []

50.times do
  address_list << { street: Faker::Address.street_address, city: Faker::Address.city, country: Faker::Address.country }
end

extra_addresses = []

15.times do
  extra_addresses << { street: Faker::Address.street_address, city: Faker::Address.city, country: Faker::Address.country }
end

workplace_list = []

20.times do
  workplace_list << { name: Faker::Company.name }
end

workplaces = workplace_list.each_with_object([]) { |attrs, ary| ary << Workplace.create(attrs) }
people = people_list.each_with_object([]) { |attrs, ary| ary << Person.create(attrs.merge({ workplace: workplaces.sample })) }

address_list.each_with_index do |attributes, idx|
  Address.create(attributes.merge(person: people[idx]))
end

extra_addresses.each do |attributes|
  Address.create(attributes.merge(person: people.sample))
end

puts 'Seeding completed.'
