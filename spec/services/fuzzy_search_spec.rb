# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Services::FuzzySearch do
  let(:subject) { described_class.new(Person) }

  context 'for single table' do
    before do
      create(:person, first_name: 'John', last_name: 'Doe', age: 25)
      create(:person, first_name: 'Johann', last_name: 'Strauss', age: 75)
      create(:person, first_name: 'Anna', last_name: 'Templehof', age: 33)
      create(:person, first_name: 'Arthur', last_name: 'Denholm', age: 21)
      create(:person, first_name: 'William', last_name: 'Blake', age: 31)
    end

    it 'filters first_name' do
      expect(subject.find_records('joh', %w[first_name]).count).to eq(2)
    end

    it 'filters last_name' do
      expect(subject.find_records('temp', %w[last_name]).count).to eq(1)
    end

    it 'filters age' do
      expect(subject.find_records('5', %w[age]).count).to eq(2)
    end

    it 'filters multiple attributes' do
      expect(subject.find_records('a', %w[first_name last_name]).count).to eq(4)
    end

    context 'when there is no pattern specified' do
      it 'returns all records' do
        expect(subject.find_records('', []).count).to eq(5)
      end
    end

    context 'when there are no columns specified' do
      it 'returns all records' do
        expect(subject.find_records('my query', []).count).to eq(5)
      end
    end
  end

  context 'for relations' do
    before do
      person_one = create(:person, first_name: 'John', last_name: 'Doe', age: 25)
      person_two = create(:person, first_name: 'Johann', last_name: 'Strauss', age: 75)

      create(:address, person: person_one, city: 'Unknown', street: 'Unknown', country: 'Unknown')
      create(:address, person: person_two, city: 'Vienna', street: 'Opernring', country: 'Austria')
    end

    it 'finds appropriate relations and their models' do
      expect(subject.relations).to eq(addresses: 'Address', workplace: 'Workplace')
    end

    it 'filters through relation attributes' do
      expect(subject.find_records('aus', %w[addresses_country]).count).to eq(1)
    end

    it 'returns the correct objects' do
      expect(subject.find_records('aus', %w[addresses_country]).first.last_name).to eq('Strauss')
    end

    it 'filters through entity and relation attributes' do
      expect(subject.find_records('a', %w[first_name last_name addresses_city]).count).to eq(1)
    end

    context 'when there are no relations to match' do
      it 'filters through base record only' do
        expect(subject.find_records('5', %w[age unrelated_column]).count).to eq(2)
      end
    end
  end
end
