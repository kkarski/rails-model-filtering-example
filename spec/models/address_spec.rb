# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Address, type: :model do
  it_behaves_like 'filterable'

  it 'is valid with valid attributes' do
    expect(build(:address)).to be_valid
  end

  it 'is not valid without a person' do
    expect(build(:address, person: nil)).not_to be_valid
  end

  it 'is not valid without a street' do
    expect(build(:address, street: nil)).not_to be_valid
  end

  it 'is not valid without a city' do
    expect(build(:address, city: nil)).not_to be_valid
  end

  it 'is not valid without a country' do
    expect(build(:address, country: nil)).not_to be_valid
  end
end
