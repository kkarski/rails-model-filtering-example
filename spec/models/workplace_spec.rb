# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Workplace, type: :model do
  it_behaves_like 'filterable'

  it 'is valid with valid attributes' do
    expect(build(:workplace)).to be_valid
  end

  it 'is not valid without a name' do
    expect(build(:workplace, name: nil)).not_to be_valid
  end
end
