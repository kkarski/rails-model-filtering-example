# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Person, type: :model do
  it_behaves_like 'filterable'

  it 'is valid with valid attributes' do
    expect(build(:person)).to be_valid
  end

  it 'is not valid without a first_name' do
    expect(build(:person, first_name: nil)).not_to be_valid
  end

  it 'is not valid without a last_name' do
    expect(build(:person, last_name: nil)).not_to be_valid
  end

  it 'is not valid with non-integer age' do
    expect(build(:person, age: 2.5)).not_to be_valid
  end

  it 'is not valid with age less than 0' do
    expect(build(:person, age: -2)).not_to be_valid
  end
end
