FactoryBot.define do
  factory :workplace do
    name { Faker::Company.name }
  end
end
