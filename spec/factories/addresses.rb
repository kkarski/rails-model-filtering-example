FactoryBot.define do
  factory :address do
    person
    city { Faker::Address.city }
    street { Faker::Address.street_address }
    country { Faker::Address.country }
  end
end
