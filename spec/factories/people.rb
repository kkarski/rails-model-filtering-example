FactoryBot.define do
  factory :person do
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    age { rand(21..65) }

    factory :person_with_addresses do
      transient do
        addresses_count { 1 }
      end

      after(:create) do |person, evaluator|
        create_list(:address, evaluator.addresses_count, person: person)
      end
    end

    factory :person_with_workplace do
      workplace
    end

    factory :person_with_workplace_and_addresses do
      workplace

      transient do
        addresses_count { 1 }
      end

      after(:create) do |person, evaluator|
        create_list(:address, evaluator.addresses_count, person: person)
      end
    end
  end
end
