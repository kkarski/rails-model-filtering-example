# frozen_string_literal: true

require 'rails_helper'

RSpec.describe PeopleController, type: :controller do
  context '#index' do
    it 'returns status 200' do
      get :index

      expect(response.status).to eq(200)
    end
  end
end
