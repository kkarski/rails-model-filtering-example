# frozen_string_literal: true

require 'spec_helper'

shared_examples_for 'filterable' do
  it 'has "filtered" method' do
    expect(described_class.respond_to?(:filtered)).to be(true)
  end
end
