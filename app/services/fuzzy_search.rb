# frozen_string_literal: true

module Services
  class FuzzySearch
    attr_reader :model, :pattern, :columns, :matched_relations

    def initialize(model)
      @model = model
    end

    def find_records(pattern, columns)
      @pattern           = model.sanitize_sql_like(pattern)
      @columns           = columns
      @matched_relations = []

      base_query = model.where(nil) # SELECT * FROM table

      return base_query if pattern.blank? || columns.blank?

      conditions = pattern_matching_queries # WHERE column ILIKE %pattern% OR other_column ILIKE %pattern%...

      join_related_tables(base_query).where(conditions).distinct # SELECT DISTINCT, LEFT OUTER JOIN relations WHERE...
    end

    # creates a Hash of relations for the model
    # in the form of { relation_name: 'ClassName' }
    def relations
      @relations ||= model
                     .reflect_on_all_associations
                     .each_with_object({}) do |relation, result|
                       result[relation.name] = relation.class_name
                     end
    end

    private

    # adds LEFT OUTER JOINs for matched relations
    def join_related_tables(base_query)
      return base_query unless matched_relations.any?

      updated_query = base_query

      matched_relations.each do |relation|
        updated_query = updated_query.left_outer_joins(relation)
      end

      updated_query
    end

    def pattern_matching_queries
      queries = nil

      columns.each do |column|
        queries = queries_for_column(queries, column)
      end

      queries
    end

    # Add a like query for column on current table or foreign table if relation is found.
    # column = name would for example create people.name while column = workplace_name would
    # create a query for workplaces.name if there is such a relation between the models
    def queries_for_column(queries, column)
      related_tables = column_relations(column)

      if related_tables.empty?
        return append_query(queries, model, column, pattern)
      end

      queries_for_related_tables(queries, column, related_tables)
    end

    def queries_for_related_tables(queries, column, related_tables)
      updated_queries = queries

      related_tables.each do |relation, related_model|
        updated_queries = append_query(
          queries,
          related_model,
          column.sub("#{relation}_", ''),
          pattern
        )
      end

      updated_queries
    end

    # add OR {query} to the previous query
    def append_query(base_query, model_class, column, pattern)
      return base_query unless can_build_query_for_column?(model_class, column)

      query = cast_and_match_column(model_class.arel_table, column, pattern)

      return query if base_query.nil?

      base_query.or(query)
    end

    # generate CAST( table.column as TEXT ) ILIKE '%search_term%' query
    def cast_and_match_column(table, column, pattern)
      cast_column_to_text(table, column).matches("%#{pattern}%")
    end

    def cast_column_to_text(table, column)
      Arel::Nodes::NamedFunction.new('CAST', [table[column.to_sym].as('TEXT')])
    end

    def column_relations(column)
      matched = relations
                .keys
                .select { |key| column.start_with?(key.to_s) }
                .uniq

      add_matched_relations(matched)

      matched.each_with_object({}) do |relation, result|
        result[relation] = relations[relation].constantize
      end
    end

    def can_build_query_for_column?(model_class, column)
      !model_class.arel_table.nil? && model_class.column_names.include?(column)
    end

    def add_matched_relations(matches)
      @matched_relations = @matched_relations.concat(matches).uniq.compact
    end
  end
end
