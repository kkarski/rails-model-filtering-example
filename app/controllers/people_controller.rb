class PeopleController < ApplicationController
  FILTERABLE_COLUMNS = %w[
    first_name
    last_name
    age
    addresses_street
    addresses_city
    addresses_country
    workplace_name
  ].freeze

  def index
    assign_people

    respond_to do |format|
      format.js
      format.html
    end
  end

  private

  def assign_people
    @people = if params[:filter]
                filtered_people
              else
                unfiltered_people
              end
  end

  def filtered_people
    Person
      .filtered(params[:filter], FILTERABLE_COLUMNS)
      .includes(:workplace)
      .includes(:addresses)
      .order(:created_at)
      .page(params[:page])
      .per(params[:per])
  end

  def unfiltered_people
    Person
      .includes(:workplace)
      .includes(:addresses)
      .order(:created_at)
      .page(params[:page])
      .per(params[:per])
  end
end
