class Address < ApplicationRecord
  belongs_to :person

  validates :street, :city, :country, presence: true
end
