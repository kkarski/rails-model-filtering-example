# frozen_string_literal: true

# Filtering functionality for ActiveRecord models
# enables fuzzy search on any column, including relations
#
# Usage:
# ModelClass.filtered('query', %w[column_1, column_2, association_column1])
module Filterable
  extend ActiveSupport::Concern

  included do
    def self.filtered(pattern, columns)
      ::Services::FuzzySearch.new(self).find_records(pattern, columns)
    end
  end
end
