class Workplace < ApplicationRecord
  has_many :people
  has_many :addresses, through: :people

  validates :name, presence: true
end
