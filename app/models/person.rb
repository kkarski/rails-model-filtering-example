class Person < ApplicationRecord
  belongs_to :workplace, optional: true

  has_many :addresses

  validates :first_name, :last_name, presence: true
  validates :age, numericality: {
    only_integer: true,
    greater_than_or_equal_to: 0
  }
end
