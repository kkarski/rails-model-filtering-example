# README

Sample Rails application to demonstrate the basic implementation of
fuzzy search across multiple tables in a relational database.

Things you may want to cover:

* Ruby version
    - 2.5.3

* System dependencies
    - rvm
    - postgresql

* Configuration
    1. Copy ```.env.example``` into ```.env```
    2. Fill out .env with correct values for your system
    3. Run ```gem install bundler```
    4. Run ```bundle install```

* Database creation
    - ```bundle exec rails db:create```

* Database initialization
    - ```bundle exec rails db:migrate```
    - ```bundle exec rails db:seed```

* How to run the test suite
    1. ```rubocop```
    2. ```rspec```
